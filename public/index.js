var user_email;
var user_name;
var prof;
var create;
var num;
var room_id;
var room_name;
var chat;
var login;
var total_room=[];
var currentUser ;
var E;
var id;
var roomsRef ;
var room_count = 0;
var room_count1 = 0;
var invite_btn;
var invite_email;

function AddNoti(content){
    if (!("Notification" in window)) {
        alert("This browser does not support desktop notification");
      }
    
      // Let's check whether notification permissions have already been granted
      else if (Notification.permission === "granted") {
        // If it's okay let's create a notification
        var notification = new Notification("A new room: "+content+" is created !");
      }
    
      // Otherwise, we need to ask the user for permission
      else if (Notification.permission !== "denied") {
        Notification.requestPermission().then(function (permission) {
          // If the user accepts, let's create a notification
          if (permission === "granted") {
            var notification = new Notification("A new room: "+content+" is created !");
          }
        });
      }
}


function init() {
    user_email = '';
    firebase.auth().onAuthStateChanged(function(user) {
        
        var menu = document.getElementById('dynamic-menu');
        user_name = document.getElementById('uname');
        currentUser = user;
        total_room = [];
        total_room[0] = "<option value='none'>---</option>\n";
        total_room[1] =  "<option value='Lobby(ID:00000000)'>Lobby(ID:00000000)</option>\n";
                        

        // Check user login
        if (user) {
            console.log("New Version 2.16");
            console.log("USER LOGIN !!!!!");
            console.log("USER LOGIN !!!!!");
            console.log("USER LOGIN !!!!!");
            login = 1;
            user_email = user.email;
            user_name.innerText = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var logoutbtn = document.getElementById('logout-btn');
            logoutbtn.addEventListener("click", function() {
                firebase.auth().signOut().then(function() {
                    alert("User sign out success!");
                    user_name.innerText = "Username";
                    login = 0;                            
                    total_room = [];
                    total_room[0] = "<option value='none'>---</option>\n";
                    total_room[1] =  "<option value='Lobby(ID:00000000)'>Lobby(ID:00000000)</option>\n";
                    document.getElementById('room').innerHTML = total_room.join('');     
                    console.log("USER SIGNOUT !!!!!");
                    console.log("USER SIGNOUT !!!!!");
                    console.log("USER SIGNOUT !!!!!");
                }).catch(function(error) {
                    alert("User sign out failed!");
                })
            }, false);
            console.log(currentUser.email);
            E = currentUser.email;
            id = E.split("@");
            roomsRef = firebase.database().ref('user_info/'+id[0]);
            firebase.database().ref('user_info/'+id[0]+'/'+"---").set({
                id: '',
                name: "---"
            });
            firebase.database().ref('user_info/'+id[0]+'/'+"Lobby(ID:00000000)").set({
                id: '00000000',
                name: "Lobby"
            });
            roomsRef.once('value')
                .then(function(snapshot) {
                    if(login == 1){
                        snapshot.forEach(function(childshot) {
                            var childData = childshot.val();
                            if(childData.name != "---" && childData.name != "Lobby"){
                                total_room[total_room.length] = "<option value='"+childData.name+"(ID:"+childData.id+")'>"+childData.name+"(ID:"+childData.id+")"+"</option>\n";
                                room_count += 1;
                            }
                        });

                        document.getElementById('room').innerHTML = total_room.join('');
                        roomsRef.on('child_added', function(data) {
                            room_count1 += 1;
                            if (room_count1 > room_count) {
                                console.log("room_count1 > room_count");
                                var childData = data.val();
                                if(childData.name != "---" && childData.name != "Lobby"){
                                    console.log("if name:"+childData.name);
                                    total_room[total_room.length] = "<option value='"+childData.name+"(ID:"+childData.id+")'>"+childData.name+"(ID:"+childData.id+")"+"</option>\n";
                                    var ind1 = total_room.indexOf("<option value='"+childData.name+"(ID:"+childData.id+")'>"+childData.name+"(ID:"+childData.id+")"+"</option>\n");
                                    var ind2 = total_room.lastIndexOf("<option value='"+childData.name+"(ID:"+childData.id+")'>"+childData.name+"(ID:"+childData.id+")"+"</option>\n");
                                    if(ind1 != ind2){
                                        console.log("POP!!!");
                                        total_room.pop();
                                    }
                                    document.getElementById('room').innerHTML = total_room.join('');
                                }else{
                                    console.log("else name:"+childData.name);
                                }
                            }
                        });
                    }else{
                        total_room = [];
                        total_room[0] = "<option value='none'>---</option>\n";
                        total_room[1] =  "<option value='Lobby(ID:00000000)'>Lobby(ID:00000000)</option>\n";
                        document.getElementById('room').innerHTML = total_room.join('');
                    }
                    
        
                })
                .catch(e => console.log(e.message));

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='game.html'>Game</a><a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
            login = 0;
            console.log("USER HASN'T LOGIN YET !!!!!");
            console.log("USER HASN'T LOGIN YET !!!!!");
            console.log("USER HASN'T LOGIN YET !!!!!");
        }
    });

    
    
    room_id = document.getElementById('roomid');
    room_name = document.getElementById('roomname');
    create = document.getElementById('create');
    chat = document.getElementById('room');

    num=Math.floor(Math.random()*90000000+10000000);
    room_id.value = num.toString();

    invite_btn = document.getElementById("Invite");
    invite_email = document.getElementById("invite");

    invite_btn.addEventListener('click', function() { 
        if(login == 1){
            if (invite_email.value != "" && chat.value != "none") {
                var email_id = invite_email.value.split("@");
                try{
                    console.log("invite");
                    var inviteRef=firebase.database().ref('user_info');
                    var roomName = chat.value.split("(");
                    var roomID = roomName[1].split(":");
                    roomID = roomID[1].split(")");
                    inviteRef.once("value")
                        .then(function(snapshot) {
                            if(snapshot.child(email_id[0]).exists()){
                                if(roomID[0] != "00000000"){
                                    firebase.database().ref('user_info/'+email_id[0]+'/'+chat.value).set({
                                        id: roomID[0],
                                        name: roomName[0]
                                    });
                                    alert("You have successfully invite him/her !");
                                    invite_email.value = "";
                                }else{
                                    alert("Everyone is in the Lobby(ID:00000000), you don't have to invite him/her again.");
                                    invite_email.value = "";
                                }
                            }else{
                                alert("The user doesn't exist !");
                                invite_email.value = "";
                            }
                        });                  
                }catch(error){
                    alert(error.message);
                    invite_email.value = "";
                }
            }else{
                if(invite_email.value == ""){
                    alert("Please fill in the email of  the user you want to invite.");
                }else{
                    alert("Please choose a room to invite others.");
                }
                invite_email.value = "";
            }
        }else{
            alert("Please login first.");
            invite_email.value = "";
        }
    });

    create.addEventListener('click', function() { 
        if(login == 1){
            if (room_name.value != "") {
                
                try{
                    firebase.database().ref('privateRoom/'+room_name.value+"(ID:"+room_id.value+")").set({
                        id: room_id.value,
                        name: room_name.value
                    });
                    currentUser = firebase.auth().currentUser;
                    E = currentUser.email;
                    id = E.split("@");
                    firebase.database().ref('user_info/'+id[0]+'/'+room_name.value+"(ID:"+room_id.value+")").set({
                        id: room_id.value,
                        name: room_name.value
                    });
                    AddNoti(room_name.value+"(ID:"+room_id.value+")");
                    room_name.value = "";
                    

                }catch(error){
                    alert(error.message);
                    room_name.value = "";
                }
                
                
                
                    
                
            }else{
                alert("Please fill in Room Name.");
                room_name.value = "";
            }
            num=Math.floor(Math.random()*9000000+1000000);
            room_id.value = num.toString();
        }else{
            alert("Please login first.");
            room_name.value = "";
        }
    });
    
    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');

    post_btn.addEventListener('click', function() {
        if(login == 1){
            if (post_txt.value != "" && chat.value != 'none') {
                /// TODO 6: Push the post to database's "com_list" node
                ///         1. Get the reference of "com_list"
                ///         2. Push user email and post data
                ///         3. Clear text field
                
                var Ref;
                if(chat.value != "Lobby(ID:00000000)"){
                    Ref = firebase.database().ref('privateRoom/'+chat.value);
                }else{
                    Ref = firebase.database().ref('lobby');
                }
                var replacestring = post_txt.value.replace(/</g,"&lt");
                replacestring = replacestring.replace(/>/g,"&gt");
                console.log(replacestring);
                var data = {
                    data: replacestring,
                    //data: post_txt.value,
                    email: user_email
                };
                Ref.push(data);
                post_txt.value = "";
                
            }else{
                post_txt.value = "";
            }
        }else{
            alert("login to join their chat !");
            post_txt.value = "";
        }
    });

    // The html code for post
    var str_before_username = "<div class='media text-muted pt-3'><div class='media-body'>";
    var str_before_username1 = "<div class='media text-muted pt-3'><div class='media-left'><img src='img/test.svg' alt='' class='media-object mr-2 rounded' style='height:32px; width:32px;'></div><div class='media-body'><strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div></div>\n";
    var str_after_content1 = "</p><div class='col'></div></div></div></div>\n";
    
    var postsRef = firebase.database().ref('lobby');
    // List for store posts html
    var total_post = [];
    total_room[0] = "<option value='none'>---</option>\n";
    total_room[1] =  "<option value='Lobby(ID:00000000)'>Lobby(ID:00000000)</option>\n";
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;
    
   var theme = document.getElementById('theme')
   theme.addEventListener('change',function(){

        var color_bg;
        var color_txtbox;
        var color_post;
        var color_font;
        var bg;
        var border;
        var bgcolor;
        var btncolor;

        if(theme.value == 'bright'){
            color_bg = "rgb(183, 183, 183)";
            color_font = "black";
            color_txtbox = "white";
            color_post = "white";
            bg = "rgb(245,245,245)";
            border = "none";
            bgcolor = "white";
            btncolor = "whitesmoke";
        }else if(theme.value == 'dark'){
            color_bg = "rgb(10,10,10)";
            color_font = "white";
            color_txtbox = "rgb(50,50,50)";
            color_post = "rgb(35,35,35)";
            bg = "rgb(25,25,25)";
            border = "solid rgb(55,55,55) 2px";
            bgcolor = "lightgray";
            btncolor = "gray";
        }

        document.getElementById('color_info').style.background = color_bg;
        document.getElementById('font1').style.color = color_font;
        document.getElementById('font2').style.color = color_font;
        document.getElementById('font3').style.color = color_font;
        document.getElementById('font4').style.color = color_font;
        document.getElementById('font5').style.color = color_font;
        document.getElementById('comment').style.background = color_txtbox;
        document.getElementById('inputbox').style.background = color_post;
        document.getElementById('comment').style.color = color_font;
        document.getElementById('contain').style.background = bg;
        document.getElementById('contain').style.borderLeft = border;
        document.getElementById('room').style.background = bgcolor;
        document.getElementById('roomname').style.background = bgcolor;
        document.getElementById('roomid').style.background = bgcolor;
        document.getElementById('invite').style.background = bgcolor;
        document.getElementById('theme').style.background = bgcolor;
        document.getElementById('Invite').style.background = btncolor;
        document.getElementById('create').style.background = btncolor;



    });
    document.getElementById("room").addEventListener('change', function(){
        if(chat.value=="none"){
            postsRef = firebase.database().ref('privateRoom/'+chat.value);
            total_post=[];
            total_post=["<div id='weng'>WWW</div><div id='Weng'>WWW</div><div id='WENG'>WWW</div>"];
        }else if(chat.value != "Lobby(ID:00000000)"){
            postsRef = firebase.database().ref('privateRoom/'+chat.value);
            total_post=[];
        }else{
            postsRef = firebase.database().ref('lobby');
            total_post=[];
        }
        
        postsRef.once('value')
        .then(function(snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///         Hint: When history post count is less then new post count, update the new and refresh html
            //total_post=["<div id='weng'>Let's Chat</div><div id='Weng'>Let's Chat</div><div id='WENG'>Let's Chat</div>"];
            
            snapshot.forEach(function(childshot) {

                if(childshot.hasChildren()!=false){
                    var childData = childshot.val();
                    if(childData.email == user_email){
                        total_post[total_post.length] = str_before_username + "<div class='row' style='margin-right:2%;'><div class='col'></div><p class='col' style='word-wrap: break-word;word-break: break-all;resize:none;width:50%;background:rgb(211,224,255); border:gray solid 1px; border-radius:5px;text-align:right;'readonly>" + childData.data + str_after_content;
                    }else{
                        total_post[total_post.length] = str_before_username1 + childData.email + "</strong><div class='row'><p class='col' style='word-wrap: break-word;word-break: break-all;resize:none;width:50%;background:white; border:gray solid 1px; border-radius:5px;'readonly>" + childData.data + str_after_content1;
                    }
                    first_count += 1;
                }
            });
            
            document.getElementById('post_list').innerHTML = total_post.join('');

            postsRef.on('child_added', function(data) {
                if(data.hasChildren()!=false){
                    second_count += 1;
                    if (second_count > first_count ) {
                        
                        var childData = data.val();
                        if(childData.email == user_email){
                            total_post[total_post.length] = str_before_username + "<div class='row' style='margin-right:2%;'><div class='col'></div><p class='col' style='word-wrap: break-word;word-break: break-all;resize:none;width:50%;background:rgb(211,224,255); border:gray solid 1px; border-radius:5px;text-align:right;'readonly>" + childData.data + str_after_content;
                        }else{
                            //here
                            total_post[total_post.length] = str_before_username1 + childData.email + "</strong><div class='row'><p class='col' style='word-wrap: break-word;word-break: break-all;resize:none;width:50%;background:white; border:gray solid 1px; border-radius:5px;'readonly>" + childData.data + str_after_content1;
                        }
                        document.getElementById('post_list').innerHTML = total_post.join('');
                    }
                }
            });
        })
    });    
}





window.onload = function() {
    init();
    
};