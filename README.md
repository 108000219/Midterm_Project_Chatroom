# Software Studio 2021 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : Mid_Chatroom

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

# 作品網址：https://project-chatroom.web.app/

## Website Detail Description
### signin.html、signin.css、signin.js
#### Sign in/Sign out
  * Sign in
  
    1. sign in with email and password
    2. sign in with Google
    3. Create a new account
    
  * Sign out
    Simply click the Logout button
### index.html、index.css、index.js
#### Lobby
  * Everyone can see the chat log, but only sign-in user can send messages in the lobby.
  
    1. User doesn't sign in
    
       ![](https://i.imgur.com/luu8adh.png)
       
    2. User has signed in
    
       ![](https://i.imgur.com/y2kUgzk.png)

#### Private room
  * only sign-in user can own the private rooms
  
    1. User doesn't sign in
    
     ![](https://i.imgur.com/DdpxsPp.png)
     
    2. User has signed in
    
     ![](https://i.imgur.com/lAmwgBz.png)

  
#### Create a new room
  * Enter room name to create a new private room, with a automatically-generate room id.
  * Only sign-in user can create a private room.
  
    ![](https://i.imgur.com/g7Ha35V.jpg)

  * If you successfully create a room , it will pop up a chrome notification.
  
    ![](https://i.imgur.com/YmUM8yO.png)


#### Invite others to a private room
  * First choose a room from chatroom , then input the user's email and click the Invite button
  
    ![](https://i.imgur.com/NwhvEIu.png)
    
  * You can't invite unregistered user
  
    ![](https://i.imgur.com/FExk1eo.png)
    
  * Only sign-in user can invite others
     

#### Change the theme
  * There are 2 themes for you to choose
  
    1. Bright
      ![](https://i.imgur.com/Cb6r4HC.jpg)
    2. Dark
      ![](https://i.imgur.com/IaCXhwK.jpg)

#### Chat
  * Only sign-in user can send message in the chat room.
  
     1. sign-in
     
     ![](https://i.imgur.com/AXA85nW.png)
     
     ![](https://i.imgur.com/hyfBaOM.png)

     2. sign-out
     
       ![](https://i.imgur.com/4Y2DPUb.png)

#### Animation
  * 3 moving WWWs
  * Only sign-in user can see the animation
  * The animation appears when you didn't select any chatroom.
  
    ![](https://i.imgur.com/uT0W4Fy.jpg)
### game.html、game.css、game.js
#### Game
* When the user doesn't log in, he/she can play the game by clicking the drop-down selection "Game".

    ![](https://i.imgur.com/mdS2IpV.jpg)

* After that they'll reach the Game page

   ![](https://i.imgur.com/HcrW5hD.png)
   
* The user can press the "ACCELERATE" button to get a upward acceleration, and when the user click the "RESTART" button, the game will restart.
    
    ![](https://i.imgur.com/JkXuIYv.png)

* When the user click the "Sign in" button, they'll go to the sign-in page.

    ![](https://i.imgur.com/E1kqB3m.jpg)

* When the user click the "Quit" button, they'll go to the main page.

    ![](https://i.imgur.com/UzaHDyq.jpg)







# Components Description : 
### index.js
1. function AddNoti(content){} 
   * Add a chrome notification.
2. logoutbtn.addEventListener("click", function() {}) 
   * Sign out and refresh the chatroom list to only --- and Lobby.
3. roomRef.once().then(function(snapshot){}).catch(e => {})
   * Add the user's private rooms too the Chatroom drop-down list when the page is loaded or a new room is created.
4. create.addEventListener('click', function(){})
   * Create a new private room.
5. post_btn.addEventListener('click', function(){})
   * Push the message and user email to the database when the user click the Submit button.
6. document.getElementById("room").addEventListener('change', function(){})
   * show the chat message from the selected room.
   * if the user select ---, then it won't show message,instead, it will show the animation(3 moving WWW)
### signin.js
1. btnLogin.addEventListener('click', function(){})
   * allow user sign in with email and password
2. btnGoogle.addEventListener('click', function(){})
   * allow user sign in with google account
3. btnSignUp.addEventListener('click', function() {})
   * allow a user to create new account
4. function create_alert(type, message){}
   * show alert message


# Other Functions Description : 
### index.js
1. theme.addEventListener('change', function(){})
   *  change the background style.
2. invite_btn.addEventListener('click', function(){})
   * Add the room to the invited user's room list, if the invition success.
   * Then the invited user can chat in that room.
### game.js
1. signin()
    * go to the sign-in page
3. quit()
    * go back to the main page
5. startGame()
    * initialize the gravity and build the basic element we need in the game.
7. restartGame()
    * reload the webpage and restart the game.
9. component(width,height,color,x,y,type)
    * build a new component and initialize its size(width、height), color,type,and position(x,y)
    * this.update = function()
        * show the component on the canvas with new size, position...
    * this.newPos = function()
        * calculate the new position of the component
    * this.hitBottom = function()
        * when the component hit the bottom set its gravity to 0
    * this.crashWith = function(otherobj)
        * detect if the component hits any obstacles
11. updateGameArea()
    * update new scene on the canvas.
13. everyinterval(n)
    * a counter to control the obstacle.
15. accelerate(n)
    * set the gravity to n
